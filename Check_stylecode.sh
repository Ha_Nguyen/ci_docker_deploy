#!/bin/bash
cpplint ./client/*.cpp
ret1=$?
cpplint ./server/*.cpp
ret2=$?
shellcheck ./*.sh
ret3=$?
[[ "${ret1}" != 0 ]] && echo "Wrong format in client" || echo "Pass coding standard in client"
[[ "${ret2}" != 0 ]] && echo "Wrong format in server" || echo "Pass coding standard in server"
[[ "${ret3}" != 0 ]] && echo "Wrong format for script" || echo "Pass coding standard for scriptt"
