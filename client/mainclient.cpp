#include <iostream>
#include "client.h"

struct connection_details {
    const char *host, *user, *password, *database;
    int port;
};

MYSQL* mysql_connection_setup(struct connection_details mysql_details) {
    MYSQL *connection = mysql_init(NULL); // mysql instance

    //connect database
    if(!mysql_real_connect(connection, mysql_details.host, mysql_details.user, mysql_details.password, mysql_details.database, mysql_details.port, NULL, 0)){
        cout << "Connection Error: " << mysql_error(connection) << std::endl;
        exit(1); 
    }

    return connection;
}

// mysql_res = mysql result
MYSQL_RES* mysql_perform_query(MYSQL *connection, const char *sql_query) {
    //send query to db
    if(mysql_query(connection, sql_query)){
        cout << "MySQL Query Error: " << mysql_error(connection) << "\n";
        exit(1);
    }

    return mysql_use_result(connection);
}

int matching(char* ptrA, char* ptrB) {
	if (strlen(ptrA) != strlen(ptrB)) {
		return 0;
	}
	else {
		while (*ptrA != '\0') {
			if (*ptrA != *ptrB) {
				return 0;
			}
			ptrA++;
			ptrB++;
		}
	}
	return 1;
} 

int main(int argc, char* argv[]) {

	if (argc < 3) {
		cout << "Please enter username and password !!!\n";
		return -1;
	}

	char* username = argv[1];
	char* password = argv[2];

	bool authorization_passed = false;

	MYSQL *con;		// the connection
    MYSQL_RES *res;	// the results
    MYSQL_ROW row;	// the results rows (array)

    struct connection_details mysqlD;
    mysqlD.host = "172.17.0.1";  		// where the mysql database is
    mysqlD.port = 3306; 				// port
    mysqlD.user = "root"; 				// user
    mysqlD.password = "tphiepbk"; 		// the password for the database
    mysqlD.database = "cba_project";	// the databse

    // connect to the mysql database
    con = mysql_connection_setup(mysqlD);

    // get the results from executing commands
    res = mysql_perform_query(con, "select * from user;");

    while ((row = mysql_fetch_row(res)) != NULL) {
		if (matching(username, row[1]) == 1 && matching(password, row[2]) == 1) {
			authorization_passed = true;
			break;
		}
    }

    // clean up the database result
    mysql_free_result(res);
    // close database connection
    mysql_close(con);

	if (authorization_passed == false) {
		cout << "Invalid username/password !!!\n";
		return -1;
	}

	try {
		int port = 8888;
		Client * c = new Client(port);
		c->run();
		delete c;
	}
	catch (const char* e) {
		cout << "Error : " << e << endl;
	}
	return 0;
}