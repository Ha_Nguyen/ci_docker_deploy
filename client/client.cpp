#include "client.h"

Client::Client(int iport) : port(iport), serverFd(-1) {
	serverFd = socket(AF_INET, SOCK_STREAM, 0);
	if (-1 == serverFd) {
		throw("create socket error!");
	}

	memset(&serverAddr,'\0',sizeof(serverAddr));
    serverAddr.sin_family=AF_INET;
    serverAddr.sin_port=htons(iport);
    serverAddr.sin_addr.s_addr=inet_addr("127.0.0.1");

	int ret;
	ret = connect(serverFd, (struct sockaddr*)&serverAddr, sizeof(serverAddr));
	if (ret < 0) {
		throw("connect server error!");
	}
}

Client::~Client() {
	cout << "close client! " << endl;
	if (-1 == serverFd) {
		close(serverFd);
	}
}

void Client::run() {
	while (true) {
		char data[MAX_SIZE];
		printf("Client:\t");
		scanf("%s", &data[0]);
		send(serverFd, data, strlen(data), 0);

		if(strcmp(data, ":exit") == 0) {
			close(serverFd);
			printf("[-]Disconnected from server.");
			exit(1);
		}

		if(recv(serverFd, data, MAX_SIZE, 0) < 0) {
			printf("[-]Error in receiving data.");
		} 
		else {
			printf("Server: %s\n", data);
		}
	}
}