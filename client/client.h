#pragma once

#include <iostream>
#include <string>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <cstring>
#include <cstdio>
#include <signal.h>
#include <unistd.h>
#include <mysql/mysql.h>
#include <string>

#define MAX_SIZE 1024
using namespace std;

class Client {
private:
	int port;
	int serverFd;
	struct sockaddr_in serverAddr;
public:
	Client(int iport = 5000);
	~Client();
	void run();

	int get_port() {return this->port;}
	void set_port(int value) {this->port = value;}

	int get_serverFd() {return this->serverFd;}
	void set_serverFd(int value) {this->serverFd = value;}
};