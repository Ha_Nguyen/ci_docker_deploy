# CBA_Project

DEK Internship Season 28 - CBA project

## How to compile :
* Run command : make all
## How to run :
* Run mysql container :
```
sudo docker pull mysql
```
```
sudo docker run -p 3306:3306 --name <container_name> -e MYSQL_ROOT_PASSWORD=<password> -d mysql:latest
```
```
sudo docker exec -it <container_name> mysql -h localhost -u root -p
```
* Create database **cba_project**
* Create table **user**
* Insert into table :
```
INSERT INTO TABLE USER(id int PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT NULL, password int NOT NULL, expire_period int);
```
     
* Split the terminal into 2 tabs :
    * Server side : ./server
    * Client side : ./client \<username> \<password>