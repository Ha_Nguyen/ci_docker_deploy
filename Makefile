all: client.o server.o
	cd client; \
	g++ -o client client.o mainclient.o -lmysqlclient
	cd server; \
	g++ -o server server.o mainserver.o -lmysqlclient

client.o:
	cd client; \
	g++ -c client.h client.cpp mainclient.cpp

server.o:
	cd server; \
	g++ -c server.h server.cpp mainserver.cpp

clean:
	cd client; \
	rm *.o *.h.gch client
	cd server; \
	rm *.o *.h.gch server
