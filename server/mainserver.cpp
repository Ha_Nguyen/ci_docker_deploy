#include <iostream> 
#include "server.h"

using namespace std;

int main()
{
	int port = 8888;
	Server *s = new Server(port);
	try	{
		s->pollRun();
	}
	catch (const char* e) {
		cout << "e = " << e << endl;
	}
	delete s;
	return 0;
}