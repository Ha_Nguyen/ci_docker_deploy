#include "server.h"

Server::Server(int iPort) : port(iPort), listen_fd(-1), clientfd(-1)
{
	//Create socket
	listen_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (listen_fd < 0)
	{
		perror("socket() failed");
		exit(-1);
	}
	 /// Allow socket descriptor to be reuseable
	rc = setsockopt(listen_fd, SOL_SOCKET,  SO_REUSEADDR, (char *)&on, sizeof(on));
	if (rc < 0)
	{
		perror("setsockopt() failed");
		close(listen_fd);
		exit(-1);
	}
	//
	rc = ioctl(listen_fd, FIONBIO, (char *)&on);
	if (rc < 0)
	{
		perror("ioctl() failed");
		close(listen_fd);
		exit(-1);
	}
	///
	memset(&addr, 0, sizeof(addr));
	addr.sin_family      = AF_INET;
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_port        = htons(port);
	rc = bind(listen_fd, (struct sockaddr *)&addr, sizeof(addr));
	if (rc < 0)
	{
		perror("bind() failed");
		close(listen_fd);
		exit(-1);
	}

	 ///
	rc = listen(listen_fd, BACKLOG);
	if (rc < 0)
	{
		perror("listen() failed");
		close(listen_fd);
		exit(-1);
	}
}

Server::~Server()
{
	// Close all sockets
	for (int i = 0; i < BACKLOG; i++){
		close(readfds[i].fd);
	}
	cout << "close server" << endl;
}

void Server::initReadfds()
{
	for (int i = 0; i < BACKLOG; i++) {
		readfds[i].fd = -1;
		readfds[i].events = POLLIN;
	}
	readfds[0].fd = listen_fd;
	readfds[0].events = POLLIN;
}

/*
int Server::getMaxFd()
{
	maxFd = -1;
	for (int i = 0; i < BACKLOG; i++) {
		if (readfds[i].fd > maxFd) {
			maxFd = readfds[i].fd;
		}
	}
	return maxFd;
}
*/

void Server::addClient(int nfds,int new_sd)
{
	readfds[nfds].fd = new_sd;
	readfds[nfds].events = POLLIN;
	nfds++;
}

void Server::removeClient(int c)
{
	readfds[c].fd = -1;
}


void Server::closeConnect( int a)
{
  	//close connect when client dis
	if (close_conn)
	{
		close(readfds[a].fd);
		readfds[a].fd = -1;
		compress_array = true;
	}
}

void Server::compressArray()
{
  ///compress array when client disconnect
	if (compress_array)
	{
		compress_array = false;
		for (ii = 0; ii < nfds; ii++)
		{
			if (readfds[ii].fd == -1)//i=1
			{
				// printf("nfds:%d\n",nfds);
				for(j = ii; j < nfds; j++)
				{
					readfds[j].fd = readfds[j+1].fd;
				}
				nfds--;
			}
			close_conn = false;
		}
	}
}

void Server::pollRun()
{
	initReadfds();
	int addrlen = sizeof(addr);
	printf("Waiting on poll()...\n");
	while (end_server==false) 
	{
		rc = poll(readfds, nfds, timeout);
		if (rc < 0)
		{
			perror("  poll() failed");
			break;
		}
		if (rc == 0)
		{
			printf("  poll() timed out.  End program.\n");
			break;
		}
		current_size = nfds;
		for (i = 0; i < current_size; i++)
		{
			//close connect when client disconnect
			closeConnect(a);
			///compress array when client disconnect
			compressArray();
			/*********************************************************/
			/* Loop through to find the descriptors that returned    */
			/* POLLIN and determine whether it's the listening       */
			/* or the active connection.                             */
			/*********************************************************/
			if(readfds[i].revents == 0)
				continue;

			/*********************************************************/
			/* If revents is not POLLIN, it's an unexpected result,  */
			/* log and end the server.                               */
			/*********************************************************/
			if(readfds[i].revents != POLLIN)
			{
				printf("  Error! revents = %d\n", readfds[i].revents);
				end_server = true;
				break;
			}
			if (readfds[i].fd == listen_fd)
			{
				/*******************************************************/
				/* Listening descriptor is readable.                   */
				/*******************************************************/
				printf("  Listening socket is readable\n");

				/*******************************************************/
				/* Accept all incoming connections that are            */
				/* queued up on the listening socket before we         */
				/* loop back and call poll again.                      */
				/*******************************************************/
				do
				{
					/*****************************************************/
					/* Accept each incoming connection. If               */
					/* accept fails with EWOULDBLOCK, then we            */
					/* have accepted all of them. Any other              */
					/* failure on accept will cause us to end the        */
					/* server.                                           */
					/*****************************************************/
					new_sd = accept(listen_fd, (struct sockaddr *)&addr, (socklen_t*)&addrlen);
					if (new_sd < 0)
					{
						if (errno != EWOULDBLOCK)
						{
							perror("  accept() failed");
							end_server = true;
						}
						break;
					}

					/*****************************************************/
					/* Add the new incoming connection to the            */
					/* pollfd structure                                  */
					/*****************************************************/
					// printf("  New incoming connection - %d\n", new_sd);
					printf("New connection , socket fd is %d , ip is : %s , port : %d \n" , new_sd , inet_ntoa(addr.sin_addr) , ntohs(addr.sin_port));

					readfds[nfds].fd = new_sd;
					readfds[nfds].events = POLLIN;
					nfds++;
					// addClient(nfds,new_sd);

					/*****************************************************/
					/* Loop back up and accept another incoming          */
					/* connection                                        */
					/*****************************************************/
				} while (new_sd != -1);
			}

			/*********************************************************/
			/* This is not the listening socket, therefore an        */
			/* existing connection must be readable                  */
			/*********************************************************/

			if(readfds[i].revents & POLLIN && readfds[i].fd > listen_fd)
			{

				// printf("  Descriptor %d is readable\n", readfds[i].fd);
				/*****************************************************/
				/* Receive data on this connection until the         */
				/* recv fails with EWOULDBLOCK. If any other         */
				/* failure occurs, we will close the                 */
				/* connection.                                       */
				/*****************************************************/
				rc = recv(readfds[i].fd, buffer, sizeof(buffer), 0);

				if (rc < 0)
				{
					if (errno != EWOULDBLOCK)
					{
						perror("  recv() failed");
						close_conn = true;
					}
					break;
				}

				/*****************************************************/
				/* Check to see if the connection has been           */
				/* closed by the client                              */
				/*****************************************************/
				if (rc == 0)
				{
					getpeername(readfds[i].fd , (struct sockaddr*)&addr , (socklen_t*)&addrlen);
					// printf("Host disconnected , ip %s , port %d \n" ,
					// 	inet_ntoa(addr.sin_addr) , ntohs(addr.sin_port));
					printf("Host %d disconnected  \n",i);
					close_conn = true;
					a= i;
					break;
				}

				buffer[rc]='\0';
				len =rc;
				b = send(readfds[i].fd, buffer, len, 0);

				printf("Message client %d,strlen:%d :%s\n",i,(int)strlen(buffer),buffer);

				if (b < 0)
				{
					perror("  send() failed");
					close_conn = true;
					break;
				}
			}  /* End of existing connection is readable             */
		} /* End of loop through pollable descriptors              */
	}
}