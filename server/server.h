#pragma once

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <poll.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include "error.h"
#include <sys/ioctl.h> 
#include <mysql/mysql.h>

using namespace std;

#define BACKLOG 4

class Server
{
private:
	int port; // port
	int listen_fd; // listen socket
	int clientfd; // client socket
	int maxFd,rc,on=1; // socket maximum descriptor
	int new_sd,nfds=1,i,j;
	int timeout = (3 * 60 * 1000);
	char buffer[80];
	int end_server = false,close_conn=false,compress_array=false;
	int current_size;
	int a,b=0;
	int len;
	int ii;
//  struct sockaddr_in clientaddr; // Client address structure
	struct sockaddr_in addr; // listen to the server address structure
	struct pollfd readfds[BACKLOG]; // poll socket structure
//  socklen_t clientaddrlen; // client address length

public:
	Server(int iPort = 5000);
	~Server();
	 // Initialize readable socket
	void initReadfds();

	 // Add client to readable socket
	void addClient(int c,int new_sd);
	 // remove client socket
	void removeClient(int c);
      //close_conn
	void closeConnect(int a);
	  //compress array
	void compressArray();  
	void pollRun();


	// get set function
	int get_port() {return this->port;}
	void set_port(int value) {this->port = value;}

	int get_listen_fd() {return this->listen_fd;}
	void set_listen_fd(int value) {this->listen_fd = value;}

	int get_clientfd() {return this->clientfd;}
	void set_clientfd(int value) {this->clientfd = value;}

	int get_maxFd() {return this->maxFd;}
	void set_maxFd(int value) {this->maxFd = value;}

	int get_rc() {return this->rc;}
	void set_rc(int value) {this->rc = value;}

	int get_current_size() {return this->current_size;}
	void set_current_size(int value) {this->current_size = value;}
};